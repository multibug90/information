# Тестирование программного обеспечения [wiki](https://git-scm.com/) Quality Assurance (QA) Engineer #  

https://github.com/fityanos/awesome-quality-assurance-roadmap

[Bug tracking system](https://en.wikipedia.org/wiki/Bug_tracking_system)  

### Bug trackers: ###   
[Jira ](https://www.atlassian.com/ru/software/jira) [Wiki](https://www.atlassian.com/ru/software/jira)  
[YouTrack](https://www.jetbrains.com/youtrack/) [Wiki](https://ru.wikipedia.org/wiki/YouTrack)  
[Redmine](https://www.redmine.org/) [Wiki](https://ru.wikipedia.org/wiki/Redmine)  

### Должности в тестировании ###  
 https://www.it-academy.by/employment/kariera/karera-testirovshchika.php  
-Junior QA  
-MIddle QA  
-Senior QA  
-Lead QA  
-QA Team Lead  
-QA Manager  



[QaLight](https://qalight.ua/ru/baza-znaniy/white-black-grey-box-testirovanie/)   
[habr](https://habr.com/ru/post/549054/)  

 ### Виды тестирования ПО ###  
 http://wiki.rosalab.ru/ru/index.php/%D0%92%D0%B8%D0%B4%D1%8B_%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F_%D0%9F%D0%9E  	
  - [Функциональное тестирование](https://en.wikipedia.org/wiki/Functional_testing)  
  - [Системное тестирование](https://en.wikipedia.org/wiki/System_testing)  
  - [Тестирование проиводительности](https://en.wikipedia.org/wiki/Software_performance_testing)  
  - [Регрессионное тестирование](https://en.wikipedia.org/wiki/Regression_testing)  
  - [Модульное тестирование](https://en.wikipedia.org/wiki/Unit_testing)  
  - [Тестирование безопасности](https://ru.wikiqube.net/wiki/Security_testing)  
  - [Тестирование локализации](https://habr.com/ru/company/alconost/blog/521330/#:~:text=%D0%95%D1%81%D0%BB%D0%B8%20%D0%BA%D0%BE%D1%80%D0%BE%D1%82%D0%BA%D0%BE%2C%20%D0%BB%D0%BE%D0%BA%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D0%BE%D0%B5%20%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5%20%E2%80%94%20%D1%8D%D1%82%D0%BE,%D0%BF%D1%80%D0%BE%D0%B2%D0%BE%D0%B4%D0%B8%D1%82%D1%81%D1%8F%20%D0%B2%D0%BE%20%D0%B2%D1%80%D0%B5%D0%BC%D1%8F%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B8%20%D0%BF%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D0%B0.)  
  - [Юзабилити тестирование](https://en.wikipedia.org/wiki/Usability_testing)  


 ### Методологии тестирования ###  
 https://xbsoftware.ru/blog/metodologii-testirovaniya-po-kakuyu-vybrat/  
  - [Каскадная модель](https://en.wikipedia.org/wiki/Waterfall_model)  
 - [V-Model](https://en.wikipedia.org/wiki/V-Model)  
 - [Инкрементная модель](https://clck.ru/NwfTi)  
 - [Спиральная модель](https://en.wikipedia.org/wiki/Spiral_model)  
 - [Agile](https://en.wikipedia.org/wiki/Agile_software_development)  
 - [Scrum](https://ru.wikipedia.org/wiki/SCRUM)  

 ### Тестовая документация ###  


